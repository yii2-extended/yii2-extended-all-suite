# yii2-extended/yii2-extended-all-suite

Metapackage that lists all the packages under the yii2-extended namespace.

![coverage](https://gitlab.com/yii2-extended/yii2-extended-all-suite/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install yii2-extended/yii2-extended-all-suite ^8`


## Included packages

| Package Pipeline | Package Coverage | Package Name |
|:-----------------|:-----------------|:-------------|
| ![coverage](https://gitlab.com/yii2-extended/yii2-export-policy-interface/badges/master/pipeline.svg?style=flat-square) | | [yii2-extended/yii2-export-policy-interface](https://gitlab.com/yii2-extended/yii2-export-policy-interface) |
| ![coverage](https://gitlab.com/yii2-extended/yii2-export-policy-datetime/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-extended/yii2-export-policy-datetime/badges/master/coverage.svg?style=flat-square) | [yii2-extended/yii2-export-policy-datetime](https://gitlab.com/yii2-extended/yii2-export-policy-datetime) |
| ![coverage](https://gitlab.com/yii2-extended/yii2-export-policy-onefile/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-extended/yii2-export-policy-onefile/badges/master/coverage.svg?style=flat-square) | [yii2-extended/yii2-export-policy-onefile](https://gitlab.com/yii2-extended/yii2-export-policy-onefile) |
| ![coverage](https://gitlab.com/yii2-extended/yii2-psr3-logger-bridge/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-extended/yii2-psr3-logger-bridge/badges/master/coverage.svg?style=flat-square) | [yii2-extended/yii2-psr3-logger-bridge](https://gitlab.com/yii2-extended/yii2-psr3-logger-bridge) |
| ![coverage](https://gitlab.com/yii2-extended/yii2-module-metadata-interface/badges/main/pipeline.svg?style=flat-square) | | [yii2-extended/yii2-module-metadata-interface](https://gitlab.com/yii2-extended/yii2-module-metadata-interface) |
| ![coverage](https://gitlab.com/yii2-extended/yii2-module-metadata-object/badges/main/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-extended/yii2-module-metadata-object/badges/main/coverage.svg?style=flat-square) | [yii2-extended/yii2-module-metadata-object](https://gitlab.com/yii2-extended/yii2-module-metadata-object) |
| ![coverage](https://gitlab.com/yii2-extended/yii2-psr6-cache-bridge/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-extended/yii2-psr6-cache-bridge/badges/master/coverage.svg?style=flat-square) | [yii2-extended/yii2-psr6-cache-bridge](https://gitlab.com/yii2-extended/yii2-psr6-cache-bridge) |
| ![coverage](https://gitlab.com/yii2-extended/yii2-psr16-simple-cache-bridge/badges/master/pipeline.svg?style=flat-square) | ![build status](https://gitlab.com/yii2-extended/yii2-psr16-simple-cache-bridge/badges/master/coverage.svg?style=flat-square) | [yii2-extended/yii2-psr16-simple-cache-bridge](https://gitlab.com/yii2-extended/yii2-psr16-simple-cache-bridge) |


## License

MIT (See [license file](LICENSE)).
